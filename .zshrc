# oh my zsh
export ZSH=/usr/share/oh-my-zsh/

if [ -f $ZSH/oh-my-zsh.sh ]; then
  source $ZSH/oh-my-zsh.sh
fi

# Enable colors and change prompt:
autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

# History in cache directory:
export HISTCONTROL=ignoreboth:erasedups
setopt HIST_IGNORE_SPACE
HISTSIZE=10000
SAVEHIST=10000
# HISTFILE=~/.cache/zsh/history
# display the time when commands were executed with the history commandwith the history command
export HISTTIMEFORMAT="%Y-%m-%d %T "

export EDITOR="lvim"
export VISUAL="$EDITOR"

export MANPAGER='lvim +Man!'
# export MANPAGER='nvim +Man!'
# export MANPAGER="nvim -c 'set ft=man' -"

export TERMINAL="alacritty"
BROWSER="$(~/.config/sxhkd/variables 'browser1' || echo 'librewolf')"
export BROWSER

export SHELLCHECK_OPTS="-e SC2319 -e SC2155 -e SC2155"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/shell/inputrc"

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
# zstyle ':completion:*' special-dirs true
zmodload zsh/complist
compinit
_comp_options+=(globdots)   # Include hidden files.

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
bindkey -s '^o' 'lfcd\n'

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line



source ~/.alias
eval "$(starship init zsh)"

function h {
  if [[ $# -eq 0 ]]; then
    history
  else
    history | grep "${@}"
  fi
}

plugins=(
  z
  zsh-syntax-highlighting
  zsh-autosuggestions
  git
)

# other plugins
# copybuffer
# Dirhistory


# ZSH_AUTOSUGGEST_STRATEGY='completion'
# ZSH_AUTOSUGGEST_STRATEGY='match_prev_cmd'


# Load zsh-syntax-highlighting; should be last.
if [ -f /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
  # source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
  # source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
  source $ZSH_CUSTOM/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
  source $ZSH_CUSTOM/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
fi

plugins=(
  z
  zsh-syntax-highlighting
  zsh-autosuggestions
  git
)

\clear

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/usr/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/usr/etc/profile.d/conda.sh" ]; then
        . "/usr/etc/profile.d/conda.sh"
    else
        export PATH="/usr/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

for p in \
  "$HOME/.scripts/own/bin/" \
  "$HOME/.scripts/own_private/bin/" \
  "$HOME/.local/bin" \
  "$HOME/.bin:$PATH" \
  "$HOME/.local/bin:$PATH" \
  "$HOME/.cargo/bin:$PATH" \
  "$HOME/.local/share/launch_in_sand_box" \
; do
  if [ -d "$p" ]; then
    export PATH="$p:$PATH";
  fi
done

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
