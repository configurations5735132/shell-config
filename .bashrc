#!/bin/bash
# ~/.bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

shopt -s globstar
shopt -s extglob
shopt -s dotglob

set -o vi
# set editing-mode vi
# set keymap vi-command
# set keymap vi-insert
bind 'set show-mode-in-prompt on'

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

export SHELLCHECK_OPTS="-e SC2319 -e SC2155 -e SC2155"


export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/shell/inputrc"

for p in \
  "$HOME/.scripts/own/bin/" \
  "$HOME/.scripts/own_private/bin/" \
  "$HOME/.local/bin" \
  "$HOME/.bin:$PATH" \
  "$HOME/.local/bin:$PATH" \
  "$HOME/.cargo/bin:$PATH" \
  "$HOME/.local/share/launch_in_sand_box" \
; do
  if [ -d "$p" ]; then
    export PATH="$p:$PATH";
  fi
done

# Default programs:
# export EDITOR="nvim"
export EDITOR="lvim"
# export EDITOR=$(
#   if command -v lvim > /dev/null; then
#     echo lvim;
#   elif command -v nvim > /dev/null; then
#     echo nvim;
#   elif command -v vim > /dev/null; then
#     echo vim;
#   elif command -v vi > /dev/null; then
#     echo vi;
#   else
#     echo nano;
#   fi
# )
export VISUAL="$EDITOR"
export TERMINAL="alacritty"
# export TERMINAL="kitty"
BROWSER="$(~/.config/sxhkd/variables 'browser1' || echo 'librewolf')"
export BROWSER

# to get the default programs from a other script
# IFS='"' read -r -a terminal <<< "$(cat ~/.bashrc | grep "export TERMINAL")"
# declare terminal=${terminal[-1]}
# IFS='"' read -r -a editor <<< "$(cat ~/.bashrc | grep "export EDITOR")"
# declare editor=${editor[-1]}
# IFS='"' read -r -a browser <<< "$(cat ~/.bashrc | grep "export BROWSER")"
# declare browser=${browser[-1]}

export MANPAGER='lvim +Man!'
# export MANPAGER='nvim +Man!'
# export MANPAGER="nvim -c 'set ft=man' -"

### HISTORY ###
# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
export HISTCONTROL=ignoreboth:erasedups
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
export HISTSIZE=3000
export HISTFILESIZE=3000
# display the time when commands were executed with the history commandwith the history command
export HISTTIMEFORMAT="%Y-%m-%d %T "

## ytfzf
# Environment Variables:
# export YTFZF_HIST=1                                      # 0 : off history
# export YTFZF_NOTI=1                                      # 0 : turn off notification
# export YTFZF_CACHE=~/.cache/ytfzf;
export YTFZF_CONFIG_DIR="$HOME/.config/ytfzf"                    # The directory to store config files
export YTFZF_CONFIG_DIR
# export YTFZF_CONFIG_FILE='$YTFZF_CONFIG_DIR/conf.sh'     # The configuration file
# export YTFZF_LOOP=0                                      # 1 : loop the selection prompt
# export YTFZF_PREF=''                                     # 22: 720p,  18: 360p (yt-dl formats)
# export YTFZF_CUR=1                                       # For status bar modules
# export YTFZF_ENABLE_FZF_DEFAULT_OPTS=0                   # 1 : fzf will use FZF_DEFAULT_OPTS
# export YTFZF_SELECTED_SUB=en                             # Set default auto caption language (eg. English)
# export YTFZF_EXTMENU=' dmenu -i -l 30'
export YTFZF_EXTMENU=' rofi -dmenu -i'
export YTFZF_PREF="22"
export YTFZF_EXTMENU=' rofi -dmenu -fuzzy -width 1500 -p "search for ytvideo"'   # To use rofi


# ##ALIAS###
# bash specific alias
alias sb="source ~/.bashrc"


if [ -e "$HOME/.alias" ]; then
  source "$HOME/.alias"
else
  alias sb="source ~/.bashrc"

  #list
  # alias ls='exa --color=always --group-directories-first'     # colored ls
  alias la='exa -a --color=always --group-directories-first'    # all files and dirs
  alias ll='exa -al --color=always --group-directories-first'   # long form
  alias l='exa -l --color=always --group-directories-first'     # all files and dirs # long form
  alias l.='ls -a | grep "^\." | tail -n +3'
  # alias tree='exa -aT --color=always --group-directories-first' # tree listing
  alias lsg='ls | grep '
  alias llg='ll | grep '
  alias lag='la | grep '
  alias l.g='ls -a | grep "^\." | tail -n +3| grep '
  alias lg='l | grep '

  # alias cp='cp -i'
  # alias mv='mv -i'
  # alias rm='rm -i'
  #systemctl
  alias sysctl='systemctl'

  # navigation
  # alias ~='cd ~' # just use cd without arguments. It'll do the same
  alias cl='cd -'
  alias ..='cd ..'
  alias ...='cd ../..'
  alias .3='cd ../../..'
  alias .4='cd ../../../..'
  alias .5='cd ../../../../..'
  alias .6='cd ../../../../../..'
  alias cdconfig='cd ~/.config'
  alias cdvifm='cd ~/.config/vifm/'
  alias cdawesome='cd ~/.config/awesome'
  alias cdi3='cd ~/.config/i3'
  alias cdqtile='cd ~/.config/qtile/'
  alias cdnvim='cd ~/.config/nvim'
  alias cdlvim='cd ~/.config/lvim'
  alias cddocument='cd ~/Documents'
  alias cddownloads='cd ~/Downloads'
  alias cdpicturs='cd ~/Pictures/'
  alias cdworkspaces='cd ~/Workspaces'
  alias cdideaw='cd ~/Workspaces/IntelliJWorkspace'
  alias cdidea='cd ~/IdeaProjects'
  alias cdvf='cd ~/.config/vifm/'
  alias cdtrash='cd ~/.local/share/Trash'
  alias cdvftrash='cd ~/.local/share/vifm/Trash'
  alias cdsxhdkh='cd ~/.config/sxhkd/'
  alias cddotfiles="cd ~/dotfiles/"

  # navigation
  alias vfconfig='vf ~/.config'
  alias vfawesome='vf ~/.config/awesome'
  alias vfi3='vf ~/.config/i3'
  alias vfqtile='vf ~/.config/qtile/'
  alias vfnvim='vf ~/.config/nvim'
  alias vfdocument='vf ~/Documents'
  alias vfdownloads='vf ~/Downloads'
  alias vfpicturs='vf ~/Public'
  alias vfworkspaces='vf ~/Workspaces'
  alias vfideaw='vf ~/Workspaces/IntelliJWorkspace'
  alias vfidea='vf ~/IdeaProjects'
  alias vfvf='vf ~/.config/vifm/'

  alias vfconfig='vf ~/.config'
  alias vfawesome='vf ~/.config/awesome'
  alias vfi3='vf ~/.config/i3'
  alias vfqtile='vf ~/.config/qtile/'
  alias vfnvim='vf ~/.config/nvim'
  alias vfdocument='vf ~/Documents'
  alias vfdownloads='vf ~/Downloads'
  alias vfpicturs='vf ~/Public'
  alias vfworkspaces='vf ~/Workspaces'
  alias vfideaw='vf ~/Workspaces/IntelliJWorkspace'
  alias vfidea='vf ~/IdeaProjects'

  alias doomsync='~/.emacs.d/bin/doom sync'

  alias q='exit'
  alias c='clear'

  #programe shortcut
  alias ht='htop'
  alias bt='bashtop'
  alias pt='sudo powertop'
  alias vt='vtop --no-mouse --theme seti'
  alias r='ranger'
  alias v='vim'
  alias nv='nvim'
  alias lv='lvim'
  alias e='emacs -nw'
  alias n='neofetch'
  alias pf='clear && paleofetch'
  alias syscat='clear && ~/scripts/terminal-eyecandies/syscat'
  alias vifm='~/.config/vifm/scripts/vifmrun'
  alias vf='~/.config/vifm/scripts/vifmrun'
  alias br='broot'
  alias md='mkdir'
  alias te='tree -C'
  alias to='touch'

  alias b='bat -p --theme ansi'
  alias bcat='b --paging=never'

  #scrcpy and adp
  alias scry='scrcpy'
  alias scry-o='scrcpy --turn-screen-off --stay-awake'
  alias adbkill='adb kill-server'

  alias emenue='cd ~/Workspaces/python/emenu/ && ./emenu.py'

  #ISIS
  #alias isis='$HOME/Downloads/Proteus\ 7\ Professional/BIN/ISIS.EXE'

  #rofi
  alias rofi-greenclip='rofi -modi "clipboard:greenclip print" -show clipboard -run-command'
  alias rofi-calc='rofi -show calc -modi calc -no-show-match -no-sort'

  ## Colorize the grep command output for ease of use (good for log files)##
  alias grep='grep --color=auto'

  #readable output
  alias df='df -h'

  # pacman
  alias pac='sudo pacman'
  alias pacs='sudo pacman -S'         # install pkgs
  alias pacr='sudo pacman -Rns'       # delite pakg with unneeded dependenceys
  alias pacsyyu='sudo pacman -Syyu'   # update only standard pkgs

  #grub update
  alias update-grub='sudo grub-mkconfig -o /boot/grub/grub.cfg'

  #quickly kill conkies
  alias kc='killall conky'

  #vim for important configuration files
  #know what you do in these files
  alias vlightdm='sudo vim /etc/lightdm/lightdm.conf'
  alias vpacman='sudo vim /etc/pacman.conf'
  alias vgrub='sudo vim /etc/default/grub'
  alias vmkinitcpio='sudo vim /etc/mkinitcpio.conf'
  alias vslim='sudo vim /etc/slim.conf'
  alias voblogout='sudo vim /etc/oblogout.conf'
  alias vmirrorlist='sudo vim /etc/pacman.d/mirrorlist'
  alias vconfgrub='sudo vim /boot/grub/grub.cfg'

  alias nvlightdm='sudo nvim /etc/lightdm/lightdm.conf'
  alias nvpacman='sudo nvim /etc/pacman.conf'
  alias nvgrub='sudo nvim /etc/default/grub'
  alias nvmkinitcpio='sudo nvim /etc/mkinitcpio.conf'
  alias nvslim='sudo nvim /etc/slim.conf'
  alias nvoblogout='sudo nvim /etc/oblogout.conf'
  alias nvmirrorlist='sudo avim /etc/pacman.d/mirrorlist'
  alias nvconfgrub='sudo nvim /boot/grub/grub.cfg'
  alias nvqtile='nvim ~/.config/qtile/config.py'
  alias nvnote='nvim ~/Documents/notes/'
  alias nvtnote='nvim ~/Documents/notes/TechNotes.txt'
  alias nvweblinks='nvim ~/Documents/textdokumente_presentations_usw/notes/weblinks'
  alias nvawesome='nvim ~/.config/awesome/rc.lua'
  alias nvautostart='nvim ~/.autostart.sh'
  alias nvalacritty='nvim ~/.config/alacritty/alacritty.yml'
  alias nvkitty='nvim ~/.config/kitty/kitty.conf'
  alias nvbashrc='nvim ~/.bashrc'
  alias nvtmux='nvim ~/.tmux.conf'
  alias nvvf='nvim ~/.config/vifm/vifmrc'
  alias nvvocab='cd ~/Downloads/Vocabletrainer/ && nvim ~/Downloads/Vocabletrainer/res/vocables/loadVocableFiles.txt'
  alias nvsxhdkh='nvim ~/.config/sxhkd/sxhkdrc'
  alias nvvar='nvim ~/.config/sxhkd/variables'
  alias nvvocab='cd ~/Vocabletrainer/ && nvim ~/Vocabletrainer/res/vocables/loadVocableFiles.txt'
  alias nvlvconf='nvim ~/.config/lvim/config.lua'

  alias lvlightdm='sudo lvim /etc/lightdm/lightdm.conf'
  alias lvpacman='sudo lvim /etc/pacman.conf'
  alias lvgrub='sudo lvim /etc/default/grub'
  alias lvmkinitcpio='sudo lvim /etc/mkinitcpio.conf'
  alias lvslim='sudo lvim /etc/slim.conf'
  alias lvoblogout='sudo lvim /etc/oblogout.conf'
  alias lvmirrorlist='sudo avim /etc/pacman.d/mirrorlist'
  alias lvconfgrub='sudo lvim /boot/grub/grub.cfg'
  alias lvqtile='lvim ~/.config/qtile/config.py'
  alias lvnote='lvim ~/Documents/notes/'
  alias lvtnote='lvim ~/Documents/notes/TechNotes.txt'
  alias lvweblinks='lvim ~/Documents/textdokumente_presentations_usw/notes/weblinks'
  alias lvawesome='lvim ~/.config/awesome/rc.lua'
  alias lvwindowrules='lvim ~/.config/awesome/modules/windowrules.lua'
  alias lvautostart='lvim ~/.autostart.sh'
  alias lvalacritty='lvim ~/.config/alacritty/alacritty.yml'
  alias lvkitty='lvim ~/.config/kitty/kitty.conf'
  alias lvbashrc='lvim ~/.bashrc'
  alias lvtmux='lvim ~/.tmux.conf'
  alias lvvf='lvim ~/.config/vifm/vifmrc'
  alias lvsxhdkh='lvim ~/.config/sxhkd/sxhkdrc'
  alias lvvar='lvim ~/.config/sxhkd/variables'
  alias lvlvconf='lvim ~/.config/lvim/config.lua'
  alias lvvocab='cd ~/Vocabletrainer/ && lvim ~/Vocabletrainer/res/vocables/loadVocableFiles.txt'

  # edit nvim config
  alias nvnvinit='nvim ~/.config/nvim/init.*'
  # lvim
  alias nvlvkeymap='nvim ~/.config/nvim/lua/keymappings.lua'
  alias nvlvplugin='nvim ~/.config/nvim/lua/plugins.lua'
  alias nvlvsettings='nvim ~/.config/nvim/lua/settings.lua'

  alias elightdm='sudo emacs -nw /etc/lightdm/lightdm.conf'
  alias epacman='sudo emacs -nw /etc/pacman.conf'
  alias egrub='sudo emacs -nw /etc/default/grub'
  alias emkinitcpio='sudo emacs -nw /etc/mkinitcpio.conf'
  alias eslim='sudo emacs -nw /etc/slim.conf'
  alias eoblogout='sudo emacs -nw /etc/oblogout.conf'
  alias emirrorlist='sudo avim /etc/pacman.d/mirrorlist'
  alias econfgrub='sudo emacs -nw /boot/grub/grub.cfg'
  alias eqtile='emacs -nw ~/.config/qtile/config.py'
  alias enote='emacs -nw ~/Documents/notes/'
  alias etnote='emacs -nw ~/Documents/notes/TechNotes.txt'
  alias eweblinks='emacs -nw ~/Documents/textdokumente_presentations_usw/notes/weblinks'
  alias eawesome='emacs -nw ~/.config/awesome/rc.lua'
  alias eautostart='emacs -nw ~/.autostart.sh'
  alias ealacritty='emacs -nw ~/.config/alacritty/alacritty.yml'
  alias ekitty='emacs -nw ~/.config/kitty/kitty.conf'
  alias ebashrc='emacs -nw ~/.bashrc'
  alias etmux='emacs -nw ~/.tmux.conf'
  alias evf='emacs -nw ~/.config/vifm/vifmrc'
  alias evocab='cd ~/Downloads/Vocabletrainer/ && emacs -nw ~/Downloads/Vocabletrainer/res/vocables/loadVocableFiles.txt'
  alias esxhdkh='emacs -nw ~/.config/sxhkd/sxhkdrc'
  alias evar='emacs -nw ~/.config/sxhkd/variables'

  alias pushdotfiles='cd $HOME/dotfiles/; ./pushdotfiles.sh'

  # edit eim config
  alias envinit='emacs -nw ~/.config/nvim/init.*'
  # lvim
  alias elvkeymap='emacs -nw ~/.config/nvim/lua/keymappings.lua'
  alias elvplugin='emacs -nw ~/.config/nvim/lua/plugins.lua'
  alias elvsettings='emacs -nw ~/.config/nvim/lua/settings.lua'


  #yacy #peer to peer search engen
  alias startYACY='$HOME/yacy_search_server/startYACY.sh'
  alias stopYACY='$HOME/yacy_search_server/stopYACY.sh'

  #keyboard repetition rate
  alias rd='xset r rate 200 60' #default repetition rate
  alias rl="xset r rate 300 30" #lowe repetition rate

  alias libreofficeappimage="$HOME/Applications/LibreOffice-fresh.full.help-x86_64.AppImage"

  alias set_brightness="$HOME/.scripts/set_brightness.sh"

  alias cdfzf=". $HOME/.scripts/own/cdfzf.sh"
  alias fdocuments=". $HOME/.scripts/own/cdfzf.sh ~/Documents/"
  alias efzf="$HOME/.scripts/own/efzf $EDITOR"
  alias lvfzf="$HOME/.scripts/own/efzf lvim"
  alias nvfzf="$HOME/.scripts/own/efzf nvim"

  alias ediff="~/.scripts/ediff.sh"

  # alias anaconda="source /opt/anaconda/bin/activate root"
  # alias anaconda_deactivate="source /opt/anaconda/bin/deactivate root"

  alias fjtb='firejail --profile=/etc/firejail/torbrowser-launcher.profile torbrowser-launcher'

  #grub update
  alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

  #add new fonts
  alias update-fc='sudo fc-cache -fv'

  #hardware info --short
  alias hw="hwinfo --short"
  alias hwb="hwinfo --short | b"

  #get fastest mirrors in your neighborhood
  alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
  alias mirrord="sudo reflector --latest 30 --number 10 --sort delay --save /etc/pacman.d/mirrorlist"
  alias mirrors="sudo reflector --latest 30 --number 10 --sort score --save /etc/pacman.d/mirrorlist"
  alias mirrora="sudo reflector -latest 30 --number 10 --sort age --save /etc/pacman.d/mirrorlist"
  #our experimental - best option for the moment
  alias mirrorx="sudo reflector --age 6 --latest 20  --fastest 20 --threads 5 --sort rate --protocol https --save /etc/pacman.d/mirrorlist"
  alias mirrorxx="sudo reflector --age 6 --latest 20  --fastest 20 --threads 20 --sort rate --protocol https --save /etc/pacman.d/mirrorlist"
  alias ram='rate-mirrors --allow-root arch | sudo tee /etc/pacman.d/mirrorlist'

  #youtube download
  alias yta-aac="yt-dlp --extract-audio --audio-format aac "
  alias yta-best="yt-dlp --extract-audio --audio-format best "
  alias yta-flac="yt-dlp --extract-audio --audio-format flac "
  alias yta-mp3="yt-dlp --extract-audio --audio-format mp3 "
  alias ytv-best="yt-dlp -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --merge-output-format mp4 "

  #gpg
  #verify signature for isos
  alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
  alias fix-gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
  #receive the key of a developer
  alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
  alias fix-gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
  alias fix-keyserver="[ -d ~/.gnupg ] || mkdir ~/.gnupg ; cp /etc/pacman.d/gnupg/gpg.conf ~/.gnupg/ ; echo 'done'"

  #hblock (stop tracking with hblock)
  #use unhblock to stop using hblock
  alias unhblock="hblock -S none -D none"

  alias usbmount="udisksctl mount --block-device"

  # alias grepMat2SupportedFormates=grep "$(l -n+2  | tr '\n' ' ' | sed 's/.*(//; s/)//; s/,//g; s/ /\\|/g; s/\./\\./g')"
  # alias getMat2SupportedFormates="f=$(mat2 -l | tail -n+2 | sed 's/.*(//; s/)//; s/,//g' | tr '\n' ' ' | sed  's/ /\\|/g; s/\./\\./g');echo ${f:-1}"


  alias mountblockdevice='udisksctl mount --block-device'
  alias mbd='mountblockdevice'
  alias unmountblockdevice='udisksctl unmount --block-device'
  alias umbd='unmountblockdevice'

  alias pipupdate="pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U"

  alias 8051winXL="wine ~/.wine/drive_c/Program\ Files\ \(x86\)/SIMsoft/8051winXL/8051winXL.EXE"

  alias lua='lua5.3'

  # shortcuts for functions
  alias sks="setKeyboardSettings"
  alias cht="cheat"
  alias rs="random_sc"

  alias set_wallpaper="~/.scripts/wallpapersetter.sh"

  alias awesome_restart="awesome-client 'awesome.restart()'"
  ##ALIAS ENDE###
fi

# cheat() { # function to use cheat sheet more comfortable from the command line (for more infos to cheat sheet look under https://cheat.sh/)
#   b=$(alias b | awk -F "'" '{print $2}')
#   local cmd
#   if [ "$1" == "-n" ]; then
#     cmd="${*:3}"
#     cmd="$(echo  "$cmd" | tr ' ' '+')"
#     cmd="curl cheat.sh/$2/$cmd"
#     cmd="$cmd | $b -n"
#     echo not important
#   else
#     cmd="${*:2}"
#     cmd="$(echo  "$cmd" | tr ' ' '+')"
#     cmd="curl cheat.sh/$1/$cmd"
#     cmd="$cmd | $b"
#   fi
#   echo "$cmd"
#   eval "$cmd"
#   # curl cheat.sh/$1 | b -p
# }

ex () { # Extractor for all kinds of archives
  if [ -f "$1" ] ; then
    case "$1" in
      *.tar.bz2)   tar xjvf "$1"    ;;
      *.tar.gz)    tar xzvf "$1"    ;;
      *.bz2)       bunzip2 "$1"     ;;
      *.rar)       unrar x "$1"     ;;
      *.gz)        gunzip "$1"      ;;
      *.tar)       tar xvf "$1"     ;;
      *.tbz2)      tar xjvf "$1"    ;;
      *.tgz)       tar xzvf "$1"    ;;
      *.zip)       unzip "$1"       ;;
      *.Z)         uncompress "$1"  ;;
      *.7z)        7z x "$1"        ;;
      *.deb)       ar x "$1"        ;;
      *.tar.xz)    tar xvf "$1"     ;;
      *.tar.zst)   unzstd "$1"      ;;
      *.xz)        unxz "$1"        ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

function timer () { # like sleep but it's prints the remaining time every second (it's limited to whole seconds)
  local t=$1
  while [ "$t" -gt 0 ]; do
    sleep 1
    t=$(("$t"-1))
    echo "remindig_time: $t s"
  done
  echo "$1 s are over"
}

function arlam () { # Prints every second the remaining time in seconds. After time up it will play a sound
  timer "$1"
  while true; do
    notify-send "             $1 s are over         "
    mpv ~/audio_and_video/97744^ALARM.mp3
  done
}

function arlam_silent () { # Prints every second the remaining time in seconds. After time up it will send pop ups until you stop the process. (I often overlook a single pop up)
  timer "$1"
  while true; do
    notify-send "             $1 s are over         "
  done
}

function setKeyboardSettings { # set my preferred keyboard settings
  if [ -f "$HOME/.keyboardsettings.sh" ]; then
    "$HOME/.keyboardsettings.sh"
  else
    xset r rate 190 70
    setxkbmap de -option caps:escape
    # Map the caps lock key to super...
    setxkbmap -option caps:super
    # But when it is pressed only once, treat it as escape.
    killall xcape 2>/dev/null ; xcape -e 'Super_L=Escape'
  fi
}

# replacement for ln -f because it doesn't work for what ever reason
function lnf () {
  if [ -e "${*: -1}" ]; then
    trash "${*: -1}"
  fi
  ln "$@"
}


addalias ()
{
  local aliaspath="$HOME/.bashrc"
  if [ -f ~/.alias ]; then
    aliaspath="$HOME/.alias"
  fi
  # TODO: add option to select if the alias should be surrounded by ' or "
  local cmd="alias $1=\"${*: 2}\""
  echo "$cmd"
  echo "$cmd" >> "$aliaspath"
  $cmd
}



# TODO: make function to add the date to a file | make it optional if it will be added to the start or end

# TODO: make a function to clean up file names -> remove spaces and replace special characters

#PS1
#PS1='[\u@\h \W]\$ '  #default
# PS1="\[$IBlack\]┌—————(\[$BWhite\]\u\[$IBlack\])—————(\[$IBlue\]\w\[$IBlack\]) \n└> \[$BRed\]$ \[$White\]"
#PS1="\[$IBlack\]\n: —————[(\[$BWhite\]\u\[$IBlack\])(\[$IBlue\]\w\[$IBlack\])] \n: > \[$BRed\]$ \[$White\]"
#PS1="\[$IBlack\]\n: [(\[$BWhite\]\u\[$IBlack\])(\[$IBlue\]\w\[$IBlack\])] \n: > \[$BRed\]$ \[$White\]"
#PS1="\[$IBlack\]\n [\[$BWhite\]\u\[$IBlack\]: \[$IBlue\]\w\[$IBlack\]] \n > \[$BRed\]$ \[$White\]"
#PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

eval "$(starship init bash)" #nice shell prompt

setKeyboardSettings

# random_sc

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$('/home/user1/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     eval "$__conda_setup"
# else
#     if [ -f "/home/user1/anaconda3/etc/profile.d/conda.sh" ]; then
#         . "/home/user1/anaconda3/etc/profile.d/conda.sh"
#     else
#         export PATH="/home/user1/anaconda3/bin:$PATH"
#     fi
# fi
# unset __conda_setup
# <<< conda initialize <<<


clear
paleofetch

function h {
  if [[ $# -eq 0 ]]; then
    history
  else
    history | grep "${@}"
  fi
}

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
